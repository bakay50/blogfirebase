import { Component, OnInit } from '@angular/core';
import {Post} from '../post/post';
import {Input} from '@angular/core';


@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() postOfItem: Post;

  constructor() { }

  ngOnInit() {
  }

  getColorPost() {
    if(this.postOfItem.loveIts > 0) {
      return 'green';
    } else if (this.postOfItem.loveIts < 0) {
      return 'red';
    } else {
      return 'black';
    }
  }

  loveIt(){
    this.postOfItem.loveIts++;
  }

  dontLoveIt() {
    this.postOfItem.loveIts--;
  }

  getNumberOfLove() {
    return this.postOfItem.loveIts;
  }


}

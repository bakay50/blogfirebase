import { Component } from '@angular/core';
import {Post} from './post/Post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  listepostes: Post[] = [];

  constructor() {
    this.populatelistePost();
  }

  populatelistePost() {
    this.listepostes.push(new Post('Mon premier post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac egestas est, quis blandit enim. Nulla non ipsum sed magna placerat elementum. Sed dictum tristique quam, in congue nunc ultrices quis. Proin tincidunt', 0));
    this.listepostes.push(new Post('Mon deuxieme post', 'Suspendisse imperdiet, leo ac aliquam feugiat, nibh turpis gravida quam, sit amet blandit velit purus in sem. Curabitur felis lacus, blandit non enim consectetur, ornare porttitor quam. Proin et sollicitudin augue', 0));
    this.listepostes.push(new Post('Encore un post', 'Donec ut lacinia magna. Proin efficitur purus sit amet vestibulum vehicula. Praesent nec sem eget leo pellentesque laoreet. Nam laoreet, lacus id vehicula vulputate, ante mi elementum risus,', 0));
    this.listepostes.push(new Post('Mon dernier post', 'Curabitur auctor at nunc ac aliquam. Sed ultrices arcu sed semper porta. Proin et tincidunt metus. Curabitur eget mattis lectus. Donec consequat,', 0));
  }

}
